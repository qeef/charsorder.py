# -*- coding: utf-8 -*-
"""Compare two files char by char checking if interesting characters are in the
proper order.
"""
from sys import argv, exit

INTERESTING_CHARACTERS = ("-", "#", "*", "!", "[", "]", ":", "/")

if __name__ == "__main__":
    if len(argv) != 3:
        print("Exactly two parameters needed!")
        exit(1)

    # check the number of interesting characters
    with open(argv[1], "r") as f1, open(argv[2], "r") as f2:
        s1 = f1.read()
        s2 = f2.read()
    d1 = dict((x, s1.count(x)) for x in INTERESTING_CHARACTERS)
    d2 = dict((x, s2.count(x)) for x in INTERESTING_CHARACTERS)
    if len(d1) != len(d2):
        print("Number of interesting characters differ!")
        exit(1)
    for ic in INTERESTING_CHARACTERS:
        if d1[ic] != d2[ic]:
            print("Number of interesting characters differ!")
            exit(1)
    # check the order of interesting characters
    last_pos = 0
    for c1 in s1:
        if c1 in INTERESTING_CHARACTERS:
            for i in range(last_pos, len(s2)):
                if s2[i] in INTERESTING_CHARACTERS:
                    if c1 != s2[i]:
                        print("Interesting characters order differ!")
                        exit(1)
                    last_pos = i + 1
                    break
