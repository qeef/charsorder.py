# charsorder.py
Compare two files char by char checking if interesting characters are in the
proper order. This project is developed under [MIT License][].

[MIT License]: ./LICENSE

# Motivation
I need to check if Markdown syntax is changed for translated files.

# Usage
```bash
python credits.md credits.cs.md
```

If the number of interesting characters or the order differ the error appear.
